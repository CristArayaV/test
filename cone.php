<?php

class cone {

    private $host = "187.234.43.12";
    private $user = "root";
    private $password = "123456";
    private $database = "test";

    //String conection
    private $cone;

    public function __construct() {
        try {
            $this->cone = mysqli_connect($this->host, $this->user, $this->password, $this->database);
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function  sqlSeleccion($sql)
    {
        try {
            $resp = mysqli_query($this->cone, $sql);
            return $resp;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }
}
